#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr
from spectrum import pyule
from spectrum import *
from pylab import plot, mean, log10

class all_spectrum(gr.sync_block):
    """
    docstring for block all_spectrum
    """
    def __init__(self, NFFT, method):
	self.NFFT=NFFT
	self.method=method
        gr.sync_block.__init__(self,
            name="all_spectrum",
            in_sig=[(np.complex64,self.NFFT)],
            out_sig=[(np.float32,self.NFFT)])


    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
	if(self.method=="burg"):
		for i in range(0,len(in0)):
			data=in0[i]
			#burg method Funciona
			p = pburg(data, order=15, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="yulewalker"):
		for i in range(0,len(in0)):
			data=in0[i]
			# yulewalker Funciona
			p = pyule(data, 15, norm='biased', NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="correlagram"):
		for i in range(0,len(in0)):
			data=in0[i]
			# correlagram Funciona
			p = pcorrelogram(data, lag=15, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	if(self.method=="MA_method"):
		for i in range(0,len(in0)):
			data=in0[i]
			# MA method Funciona
			p = pma(data, 15, 30, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="ARMA"):
		for i in range(0,len(in0)):
			data=in0[i]
			# ARMA method Funciona
			p = parma(data, 15, 15, 30, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="covar"):
		for i in range(0,len(in0)):
			data=in0[i]
			#covar method Funciona
			p = pcovar(data, 15, NFFT=self.NFFT)	
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c	
	elif(self.method=="modcovar"):
		for i in range(0,len(in0)):
			data=in0[i]
			#modcovar method Funciona
			p = pmodcovar(data, 15, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="minvar"):
		for i in range(0,len(in0)):
			data=in0[i]
			#minvar Funciona
			p = pminvar(data, 15, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="music"):
		for i in range(0,len(in0)):
			data=in0[i]
			#music Funciona
			p = pmusic(data, 15, 11, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
	elif(self.method=="ev"):
		for i in range(0,len(in0)):
			data=in0[i]
			#ev
			p = pev(data, 15, 11, NFFT=self.NFFT)
			p();
			a=p.psd[0:self.NFFT/2]
			b=p.psd[self.NFFT/2:self.NFFT]
			c=np.concatenate([b, a])
			out[i]=c
        return len(output_items[0])
	

