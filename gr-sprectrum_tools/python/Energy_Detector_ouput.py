#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import scipy
import scipy.special as scs
import numpy
from gnuradio import gr
from pylab import log10

class Energy_Detector_ouput(gr.decim_block):
    """
    docstring for block Energy_Detector_ouput
    """
    def __init__(self, samples,Pfa):
	self.samples = samples
	self.Pfa = Pfa
	self.size=self.samples/self.samples
        gr.decim_block.__init__(self,
            name="Energy_Detector_ouput",
            in_sig=[(numpy.float32,self.samples) ,(numpy.float32,self.samples)],
            out_sig=[numpy.float32], decim=self.size)


    def work (self,input_items,output_items):
	in0 = input_items[0]
        in1 = input_items[1]
	out = output_items[0]
        # <+signalprocessinghere+>
	#float(1/10**(float(snr)/20))
	for i,j in zip(in0,in1):
		Avg = scipy.mean(i)
		signalAvg=round(Avg,8)
		NoisePower=j
		NoiseAvg=scipy.mean(NoisePower)
		var=scipy.var(NoisePower)
		stdev=scipy.sqrt(var)
		Qinv=scipy.sqrt(2)*scs.erfinv(1-2*self.Pfa)
		Threshold =round((NoiseAvg + Qinv*stdev),8)
		print(10*log10(Threshold))
		detection=-1
		if signalAvg > Threshold:
		    out[:]= 1.0
		    #print(”signalispresent” , signalAvg)
		    detection = 1
		    #print('1')
		else:
		    if(numpy.isnan(Threshold)):
		        out[:]=-1.0
		    else:
		        out[:]=0.0
		        detection= 0
		        #print('0')
        return len(output_items[0])

