#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr
from spectrum import pyule
from spectrum import *
from pylab import plot, mean, log10

class covariance(gr.sync_block):
    """
    docstring for block covariance
    """
    def __init__(self, order,NFFT,sampling,scale_by_freq):
	self.order=order
	self.NFFT=NFFT
	self.sampling=sampling
	self.scale_by_freq=scale_by_freq
        gr.sync_block.__init__(self,
            name="covariance",
            in_sig=[(np.complex64,self.NFFT)],
            out_sig=[(np.float32,self.NFFT)])


    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
	for i in range(0,len(in0)):
		data=in0[i]
		#p = pcovar(data, 15, NFFT=1024)
		p = pcovar(data, order=self.order, NFFT=self.NFFT, sampling=self.sampling, scale_by_freq=self.scale_by_freq)		
		p();
		a=p.psd[0:self.NFFT/2]
		b=p.psd[self.NFFT/2:self.NFFT]
		c=np.concatenate([b, a])
		out[i]=c
        return len(output_items[0])

