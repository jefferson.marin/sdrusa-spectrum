#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import scipy
import scipy.special as scs
import numpy
from gnuradio import gr
from pylab import log10

class Energy_DetectorP_ff(gr.decim_block):
    """
    docstring for block Energy_DetectorP_ff
    """
    def __init__(self, samples,Pfa):
	self.samples=samples
	self.pfa=Pfa
        gr.decim_block.__init__(self,
            name="Energy_DetectorP_ff",
            in_sig=[numpy.float32,numpy.float32],
            out_sig=[numpy.float32], decim=self.samples)


    def work(self, input_items, output_items):
        in0 = input_items[0]
	in1= input_items[1]
        out = output_items[0]
    	i=in0
	j=in1
	Avg = scipy.mean(i)
	signalAvg=round(Avg,8)
	#NoisePower=j**2
	NoisePower=j**2
	NoiseAvg=scipy.mean(NoisePower)
	var=scipy.var(NoisePower)
	stdev=scipy.sqrt(var)
	print("signal power",10*log10(signalAvg))
	print("noise power",10*log10(NoiseAvg))
	Qinv=scipy.sqrt(2)*scs.erfinv(1-2*self.pfa)
	Threshold =round((NoiseAvg + Qinv*stdev),8)
	print("Threshold",10*log10(Threshold))
	detection=-1
	if signalAvg > Threshold:
	    out[:]= 1.0
	    #print(”signalispresent” , signalAvg)
	    detection = 1
	    #print('1')
	else:
	    if(numpy.isnan(Threshold)):
	        out[:]=-1.0
	    else:
	        out[:]=0.0
	        detection= 0
	        #print('0')
        return len(output_items[0])

