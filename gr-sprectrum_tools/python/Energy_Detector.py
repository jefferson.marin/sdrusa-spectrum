#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import scipy
import scipy.special as scs
import numpy
from gnuradio import gr

import csv
import calendar
import time

class Energy_Detector(gr.sync_block):
    """
    docstring for block Energy_Detector
    """
    def __init__(self, samples,Pfa,file_ouput):
	self.samples = samples
	self.Pfa = Pfa
	self.file_ouput=file_ouput

	timestamp=calendar.timegm(time.gmtime())
	self.out_file=open(self.file_ouput,'a')
	self.writer=csv.writer(self.out_file)
	#self.writer.writerow(('prediction','Threshold','signalAvg','NoiseAvg','stdev',str(timestamp)))

        gr.sync_block.__init__(self,
            name="Energy_Detector",
            in_sig=[(numpy.float32,self.samples) ,(numpy.float32,self.samples)],
            out_sig=None)


    def work (self,input_items,output_items):
	in0 = input_items[0]
        in1 = input_items[1]
	#out = output_items[0]
        # <+signalprocessinghere+>
	#float(1/10**(float(snr)/20))
	for i,j in zip(in0,in1):
		Avg = scipy.mean(i)
		signalAvg=round(Avg,8)
		NoisePower=j
		NoiseAvg=scipy.mean(NoisePower)
		var=scipy.var(NoisePower)
		stdev=scipy.sqrt(var)
		Qinv=scipy.sqrt(2)*scs.erfinv(1-2*self.Pfa)
		Threshold =round((NoiseAvg + Qinv*stdev),8)
		detection=-1
		if signalAvg > Threshold:
		    #out[:]= 1.0
		    #print(”signalispresent” , signalAvg)
		    detection = 1
		    #print('1')
		else:
		    if(numpy.isnan(Threshold)):
		        detection=-1
		    else:
		        detection= 0
		        #print('0')	
		self.writer.writerow((detection,Threshold,signalAvg,NoiseAvg,stdev))
	#self.out_file.close()
        return len(input_items[0])

