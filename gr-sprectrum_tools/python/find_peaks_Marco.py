#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy
from gnuradio import gr
from scipy.signal import find_peaks

import csv
import calendar
import time


class find_peaks_Marco(gr.decim_block):
    """
    docstring for block find_peaks_Marco
    """
    def __init__(self, FFT_len,bandwidth,window_len,value,file_ouput):
	self.size=FFT_len/FFT_len
	self.bandwidth=bandwidth
	self.FFT_len=FFT_len
	self.window_len=window_len
	self.value=value

	self.file_ouput=file_ouput
	timestamp=calendar.timegm(time.gmtime())
	self.out_file=open(self.file_ouput,'a')
	self.writer=csv.writer(self.out_file)
	self.writer.writerow(('peak_1','peak_2','decision',str(timestamp)))


        gr.decim_block.__init__(self,
            name="find_peaks_Marco",
            in_sig=[(numpy.float32,self.FFT_len)],
            out_sig=[numpy.float32], decim=self.size)


    def work (self,input_items,output_items):
	in0 = input_items[0]
	out = output_items[0]
        # <+signalprocessinghere+>
	for i in in0:
		data=i
		bw_n_pos=self.bandwidth/self.FFT_len
		n_window_elements=int(self.window_len/bw_n_pos)
		n_iterations=int(self.FFT_len/n_window_elements)
		pos_initial=0
		pos_final=n_window_elements
		peaks_value=[]
		for j in range(0,n_iterations):
		    if(j==n_iterations-1):
			sub_data=numpy.asarray(data[pos_initial:self.FFT_len])
			peaks, _ = find_peaks(sub_data, height=-100)
			max_peak=numpy.sort(sub_data[peaks])
			max_peak=max_peak[::-1]
			if(len(max_peak)!=0):
			    peaks_value.append(max_peak[0])
			if(n_iterations==1):
			    peaks_value.append(max_peak[1])
		    else:
			sub_data=numpy.asarray(data[pos_initial:pos_final])
			peaks, _ = find_peaks(sub_data, height=-100)
			pos_initial=pos_final
			pos_final+=n_window_elements
			max_peak=numpy.sort(sub_data[peaks])
			max_peak=max_peak[::-1]
			if(len(max_peak)!=0):
			    peaks_value.append(max_peak[0])
		max_peak=numpy.sort(peaks_value)
		max_peak=max_peak[::-1]
		peak_1=0.0
		peak_2=0.0
		if(len(max_peak)!=0 and len(max_peak)!=1):
			peak_1=max_peak[0]
			peak_2=max_peak[1]
		#print(peak_1,peak_2)
		decision=0.0
		if(peak_2!=0 and (peak_1/peak_2) > self.value):
		    out[:]= 1.0
		    decision=1.0
		else:
		    out[:]=0.0
		    decision=0.0
		self.writer.writerow((peak_1,peak_2,decision))
        return len(output_items[0])

