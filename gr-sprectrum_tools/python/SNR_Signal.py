#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import scipy
import scipy.special as scs
import numpy
from gnuradio import gr
from pylab import log10

class SNR_Signal(gr.decim_block):
    """
    docstring for block SNR_Signal
    """
    def __init__(self, samples,size):
	self.samples = samples
	self.size=size
        gr.decim_block.__init__(self,
            name="SNR_Signal",
            in_sig=[(numpy.float32,self.samples) ,(numpy.float32,self.samples)],
            out_sig=[numpy.float32,numpy.float32,numpy.float32], decim=self.size)


    def work (self,input_items,output_items):
	in0 = input_items[0]
        in1 = input_items[1]
	out0 = output_items[0]
	out1 = output_items[1]
	out2 = output_items[2]
	for i,j in zip(in0,in1):
		Avg_signal = scipy.mean(i)
		Avg_noise = scipy.mean(j)
		Avg_signal = round(Avg_signal,8)
		Avg_noise = round(Avg_noise,8)
		out0[:]=10*log10(Avg_signal)
		out1[:]=10*log10(Avg_noise)
		if(Avg_noise==0):
		    out2[:]=0.0
		else:
		    out2[:]=10*log10(Avg_signal/Avg_noise)
        return len(output_items[0])

