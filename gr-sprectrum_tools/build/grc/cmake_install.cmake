# Install script for directory: /home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gnuradio/grc/blocks" TYPE FILE FILES
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_yule_walker.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_ARMA.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_burg.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_covariance.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_periodogram.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_all_spectrum.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_music.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_Correlogram.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_Ma.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_pmodcovar.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_minvar.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_ev.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_DaniellPeriodogram.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_find_peaks_Marco.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_normalize.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_add_const_vector.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_Energy_DetectorP_ff.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_Energy_Detector.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_Energy_Detector_ouput.xml"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/grc/sprectrum_tools_SNR_Signal.xml"
    )
endif()

