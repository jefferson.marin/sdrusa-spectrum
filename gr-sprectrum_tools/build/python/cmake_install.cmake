# Install script for directory: /home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages/sprectrum_tools" TYPE FILE FILES
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/__init__.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/yule_walker.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/ARMA.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/burg.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/covariance.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/periodogram.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/all_spectrum.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/music.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/Correlogram.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/Ma.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/pmodcovar.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/minvar.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/ev.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/DaniellPeriodogram.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/find_peaks_Marco.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/normalize.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/add_const_vector.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/Energy_DetectorP_ff.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/Energy_Detector.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/Energy_Detector_ouput.py"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python/SNR_Signal.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages/sprectrum_tools" TYPE FILE FILES
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/__init__.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/yule_walker.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/ARMA.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/burg.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/covariance.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/periodogram.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/all_spectrum.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/music.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Correlogram.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Ma.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/pmodcovar.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/minvar.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/ev.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/DaniellPeriodogram.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/find_peaks_Marco.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/normalize.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/add_const_vector.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Energy_DetectorP_ff.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Energy_Detector.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Energy_Detector_ouput.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/SNR_Signal.pyc"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/__init__.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/yule_walker.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/ARMA.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/burg.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/covariance.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/periodogram.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/all_spectrum.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/music.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Correlogram.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Ma.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/pmodcovar.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/minvar.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/ev.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/DaniellPeriodogram.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/find_peaks_Marco.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/normalize.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/add_const_vector.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Energy_DetectorP_ff.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Energy_Detector.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/Energy_Detector_ouput.pyo"
    "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/SNR_Signal.pyo"
    )
endif()

