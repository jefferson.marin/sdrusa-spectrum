# CMake generated Testfile for 
# Source directory: /home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/python
# Build directory: /home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(qa_yule_walker "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_yule_walker_test.sh")
add_test(qa_ARMA "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_ARMA_test.sh")
add_test(qa_burg "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_burg_test.sh")
add_test(qa_covariance "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_covariance_test.sh")
add_test(qa_periodogram "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_periodogram_test.sh")
add_test(qa_all_spectrum "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_all_spectrum_test.sh")
add_test(qa_music "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_music_test.sh")
add_test(qa_Correlogram "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_Correlogram_test.sh")
add_test(qa_find_peaks "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_find_peaks_test.sh")
add_test(qa_marco_criteria "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_marco_criteria_test.sh")
add_test(qa_add_vector "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_add_vector_test.sh")
add_test(qa_Ma "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_Ma_test.sh")
add_test(qa_pmodcovar "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_pmodcovar_test.sh")
add_test(qa_minvar "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_minvar_test.sh")
add_test(qa_ev "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_ev_test.sh")
add_test(qa_DaniellPeriodogram "/bin/sh" "/home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build/python/qa_DaniellPeriodogram_test.sh")
