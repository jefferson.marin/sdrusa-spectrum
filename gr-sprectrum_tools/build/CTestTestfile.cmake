# CMake generated Testfile for 
# Source directory: /home/jefferson/sdrusa-spectrum/gr-sprectrum_tools
# Build directory: /home/jefferson/sdrusa-spectrum/gr-sprectrum_tools/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("include/sprectrum_tools")
subdirs("lib")
subdirs("swig")
subdirs("python")
subdirs("grc")
subdirs("apps")
subdirs("docs")
