# gr-sprectrum_tools 
gr-sprectrum_tools es la implementación de un sistema de detección y estimación espectral de señales usando Software Defined Radio (SDR).

# Bloques Implementados.

Cabe resaltar que el sistema de detección y estimación espectral uso de la librería spectrum de Python para implementar los métodos de estimación espectral a través de los siguientes bloques:

**Métodos de Estimación Espectral Implementados.**

gr-sprectrum_tools implementa los siguientes métodos de estimación del espectro como bloque en GNU Radio.
- all_spectrum
- ARMA
- burg
- Correlogram
- covariance
- DaniellPeriodogram
- ev
- Ma
- minvar
- music
- periodogram
- pmodcovar
- yule_walker

los bloques restantes son algoritmos implementados y se agrupan de la siguiente forma: 

**Detector de energía.**
- Energy_Detector
- Energy_Detector_ouput
- Energy_DetectorP_ff 

**Bloque para obtener los valores de la relación señal/ruido (SNR).** 
- SNR_Signal

**Operaciones con señales.** 
- add_const_vector
- normalize

## Instalación.

Para la implementación de un sistema de detección y estimación espectral de señales usando Software Defined Radio (SDR), se hace uso de GNU Radio creando módulos OOT (OutOfTreeModules), los módulos están escritos en Python, y hacen uso de la librería spectrum.

## Composición del proyecto.

El proyecto cuenta con versionamiento y posee tres ramas: 

- **master**: es la rama en la cual se publican versiones tipo *release* de la aplicación y es la rama principal dentro del ambiente de producción.

- **develop**: es la rama principal de desarrollo de la cual se desprenden todos los features de la aplicación, esta en constante actividad y contiene versiones tipo *debug* de la aplicación

- **test**: es la rama en la cual se llevan a cabo las pruebas unitarias y de integración de la aplicación.

### Dependencias:

- [GNU Radio versión 3.7](https://github.com/gnuradio/gnuradio).

`sudo apt install gnuradio`
- [Spectrum: a Spectral Analysis Library in Python](https://pyspectrum.readthedocs.io/en/latest/index.html)

`pip install spectrum`



### Instalación de gr-sprectrum_tools:

```
git clone https://gitlab.com/jefferson.marin/sdrusa-spectrum.git
cd sdrusa-spectrum
mkdir build
cd build
cmake ../
make
sudo make install
sudo ldconfig
```